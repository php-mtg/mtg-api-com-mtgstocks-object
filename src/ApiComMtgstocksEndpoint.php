<?php declare(strict_types=1);

/*
 * This file is part of the php-mtg/mtg-api-com-mtgstocks-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpMtg\ApiComMtgstocks;

use InvalidArgumentException;
use PhpExtended\DataProvider\JsonStringDataProvider;
use PhpExtended\DataProvider\UnprovidableThrowable;
use PhpExtended\HttpMessage\RequestFactory;
use PhpExtended\HttpMessage\StreamFactory;
use PhpExtended\HttpMessage\UriFactory;
use PhpExtended\Parser\ParseThrowable;
use PhpExtended\Reifier\ReificationThrowable;
use PhpExtended\Reifier\Reifier;
use PhpExtended\Reifier\ReifierInterface;
use Psr\Http\Client\ClientExceptionInterface;
use Psr\Http\Client\ClientInterface;
use Psr\Http\Message\RequestFactoryInterface;
use Psr\Http\Message\StreamFactoryInterface;
use Psr\Http\Message\UriFactoryInterface;
use RuntimeException;
use Throwable;

/**
 * ApiComMtgstocksEndpoint class file.
 * 
 * This class is a simple implementation of the ApiComMtgstocksEndpointInterface.
 * 
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.CouplingBetweenObjects")
 */
class ApiComMtgstocksEndpoint implements ApiComMtgstocksEndpointInterface
{
	
	public const HOST = 'https://api.mtgstocks.com/';
	
	/**
	 * The http client.
	 *
	 * @var ClientInterface
	 */
	protected ClientInterface $_httpClient;
	
	/**
	 * The uri factory.
	 *
	 * @var UriFactoryInterface
	 */
	protected UriFactoryInterface $_uriFactory;
	
	/**
	 * The request factory.
	 *
	 * @var RequestFactoryInterface
	 */
	protected RequestFactoryInterface $_requestFactory;
	
	/**
	 * The stream factory.
	 *
	 * @var StreamFactoryInterface
	 */
	protected StreamFactoryInterface $_streamFactory;
	
	/**
	 * The reifier.
	 *
	 * @var ReifierInterface
	 */
	protected ReifierInterface $_reifier;
	
	/**
	 * @var array<integer, ApiComMtgstocksFormat>
	 */
	protected array $_formats = [];
	
	/**
	 * Constructor of the instance. This object acts as a coordinator between
	 * all the modules of the mioga application.
	 *
	 * This method builds the object but does not connect to it, so it never
	 * throws exceptions.
	 *
	 * @param ClientInterface $client
	 * @param ?UriFactoryInterface $uriFactory
	 * @param ?RequestFactoryInterface $requestFactory
	 * @param ?StreamFactoryInterface $streamFactory
	 * @param ?ReifierInterface $reifier
	 */
	public function __construct(
		ClientInterface $client,
		?UriFactoryInterface $uriFactory = null,
		?RequestFactoryInterface $requestFactory = null,
		?StreamFactoryInterface $streamFactory = null,
		?ReifierInterface $reifier = null
	) {
		$this->_httpClient = $client;
		$this->_uriFactory = $uriFactory ?? new UriFactory();
		$this->_requestFactory = $requestFactory ?? new RequestFactory();
		$this->_streamFactory = $streamFactory ?? new StreamFactory();
		$this->_reifier = $reifier ?? new Reifier();
		
		$configuration = $this->_reifier->getConfiguration();
		
		$configuration->setIterableInnerTypes(ApiComMtgstocksArchetype::class, ['colors'], 'string');
		$configuration->setIterableInnerTypes(ApiComMtgstocksArchetypePrez::class, ['decks'], ApiComMtgstocksDeckAttributes::class);
		$configuration->setIterableInnerTypes(ApiComMtgstocksCard::class, ['splitcost', 'splitcost2'], 'string');
		$configuration->setIterableInnerTypes(ApiComMtgstocksDeck::class, ['commanders'], ApiComMtgstocksCard::class);
		$configuration->setIterableInnerTypes(ApiComMtgstocksDeck::class, ['mainboard', 'sideboard'], ApiComMtgstocksCardQuantity::class);
		$configuration->setIterableInnerTypes(ApiComMtgstocksDeckCard::class, ['colors', 'colorIdentity'], 'string');
		$configuration->setIterableInnerTypes(ApiComMtgstocksInterestCollection::class, ['foil', 'normal'], ApiComMtgstocksInterest::class);
		$configuration->setIterableInnerTypes(ApiComMtgstocksPriceHistory::class, ['avg', 'foil', 'high', 'low', 'market', 'market_foil'], ApiComMtgstocksPrice::class);
		$configuration->setIterableInnerTypes(ApiComMtgstocksPrinting::class, ['sets'], ApiComMtgstocksSetOwning::class);
		$configuration->setIterableInnerTypes(ApiComMtgstocksSet::class, ['prints'], ApiComMtgstocksPrintingPrez::class);
		$configuration->setIterableInnerTypes(ApiComMtgstocksTournament::class, ['decks'], ApiComMtgstocksDeckPlacement::class);
		
		$configuration->addFieldNameAlias(ApiComMtgstocksPrice::class, 'date', '0');
		$configuration->addFieldNameAlias(ApiComMtgstocksPrice::class, 'value', '1');
		
		$configuration->addDateTimeFormat(ApiComMtgstocksTournament::class, 'date', [
			'Y-m-d\\TH:i:s.v\\Z', // php7.3+
			'Y-m-d\\TH:i:s.???\\Z', // php7.2, php7.1
		]);
		
		// 		$configuration->setPolymorphismKey(ApiComMtgstocksPagination::class, 'type');
		// 		$configuration->setPolymorphismClass(ApiComMtgstocksPagination::class, 'card', ApiComMtgstocksCard::class);
		
		$configuration->addFieldNameAlias(ApiComMtgstocksPrinting::class, 'imageFlip', 'flipImage');
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\ApiComMtgstocks\ApiComMtgstocksEndpointInterface::getSetsInformation()
	 * @throws ClientExceptionInterface
	 * @throws InvalidArgumentException should not happen
	 * @throws ReificationThrowable
	 * @throws UnprovidableThrowable
	 */
	public function getSetsInformation() : array
	{
		$uri = $this->_uriFactory->createUri(self::HOST.'card_sets');
		$request = $this->_requestFactory->createRequest('GET', $uri);
		$response = $this->_httpClient->sendRequest($request);
		$json = new JsonStringDataProvider($response->getBody()->__toString());
		
		return $this->_reifier->reifyAll(ApiComMtgstocksSetPrez::class, $json->provideAll());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\ApiComMtgstocks\ApiComMtgstocksEndpointInterface::getSetPrintings()
	 * @throws ClientExceptionInterface
	 * @throws InvalidArgumentException should not happen
	 * @throws ReificationThrowable
	 * @throws UnprovidableThrowable
	 */
	public function getSetPrintings(int $setId) : ApiComMtgstocksSet
	{
		$uri = $this->_uriFactory->createUri(self::HOST.'card_sets/'.((string) $setId));
		$request = $this->_requestFactory->createRequest('GET', $uri);
		$response = $this->_httpClient->sendRequest($request);
		$json = new JsonStringDataProvider($response->getBody()->__toString());
		
		return $this->_reifier->reify(ApiComMtgstocksSet::class, $json->provideOne());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\ApiComMtgstocks\ApiComMtgstocksEndpointInterface::getPrinting()
	 * @throws ClientExceptionInterface
	 * @throws InvalidArgumentException should not happen
	 * @throws ReificationThrowable
	 * @throws UnprovidableThrowable
	 */
	public function getPrinting(int $printingId) : ApiComMtgstocksPrinting
	{
		$uri = $this->_uriFactory->createUri(self::HOST.'prints/'.((string) $printingId));
		$request = $this->_requestFactory->createRequest('GET', $uri);
		$response = $this->_httpClient->sendRequest($request);
		$json = new JsonStringDataProvider($response->getBody()->__toString());
		
		return $this->_reifier->reify(ApiComMtgstocksPrinting::class, $json->provideOne());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\ApiComMtgstocks\ApiComMtgstocksEndpointInterface::getPrintingPrices()
	 * @throws ClientExceptionInterface
	 * @throws InvalidArgumentException should not happen
	 * @throws ReificationThrowable
	 * @throws UnprovidableThrowable
	 */
	public function getPrintingPrices(int $printingId) : ApiComMtgstocksPriceHistory
	{
		$uri = $this->_uriFactory->createUri(self::HOST.'prints/'.((string) $printingId).'/prices');
		$request = $this->_requestFactory->createRequest('GET', $uri);
		$response = $this->_httpClient->sendRequest($request);
		$json = new JsonStringDataProvider($response->getBody()->__toString());
		
		return $this->_reifier->reify(ApiComMtgstocksPriceHistory::class, $json->provideOne());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\ApiComMtgstocks\ApiComMtgstocksEndpointInterface::getPrintingLinks()
	 * @throws ClientExceptionInterface
	 * @throws InvalidArgumentException should not happen
	 * @throws ReificationThrowable
	 * @throws UnprovidableThrowable
	 */
	public function getPrintingLinks(int $printingId) : array
	{
		$uri = $this->_uriFactory->createUri(self::HOST.'prints/'.((string) $printingId).'/rel_links');
		$request = $this->_requestFactory->createRequest('GET', $uri);
		$response = $this->_httpClient->sendRequest($request);
		$json = new JsonStringDataProvider($response->getBody()->__toString());
		
		return $this->_reifier->reifyAll(ApiComMtgstocksLink::class, $json->provideAll());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\ApiComMtgstocks\ApiComMtgstocksEndpointInterface::getCardArchetypePresences()
	 * @throws ClientExceptionInterface
	 * @throws InvalidArgumentException should not happen
	 * @throws ReificationThrowable
	 * @throws UnprovidableThrowable
	 */
	public function getCardArchetypePresences(int $cardId) : array
	{
		$uri = $this->_uriFactory->createUri(self::HOST.'prints/'.((string) $cardId).'/decks');
		$request = $this->_requestFactory->createRequest('GET', $uri);
		$response = $this->_httpClient->sendRequest($request);
		$json = new JsonStringDataProvider($response->getBody()->__toString());
		
		return $this->_reifier->reifyAll(ApiComMtgstocksArchetypeStat::class, $json->provideAll());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\ApiComMtgstocks\ApiComMtgstocksEndpointInterface::getDeck()
	 * @throws ClientExceptionInterface
	 * @throws InvalidArgumentException should not happen
	 * @throws ReificationThrowable
	 * @throws UnprovidableThrowable
	 */
	public function getDeck(int $deckId) : ApiComMtgstocksDeck
	{
		$uri = $this->_uriFactory->createUri(self::HOST.'decks/'.((string) $deckId));
		$request = $this->_requestFactory->createRequest('GET', $uri);
		$response = $this->_httpClient->sendRequest($request);
		$json = new JsonStringDataProvider($response->getBody()->__toString());
		
		return $this->_reifier->reify(ApiComMtgstocksDeck::class, $json->provideOne());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\ApiComMtgstocks\ApiComMtgstocksEndpointInterface::getDecksByArchetypeAndPrinting()
	 * @throws ClientExceptionInterface
	 * @throws InvalidArgumentException should not happen
	 * @throws ReificationThrowable
	 * @throws UnprovidableThrowable
	 */
	public function getDecksByArchetypeAndPrinting(int $archetypeId, int $printingId, int $pagenb = 1) : array
	{
		$uri = $this->_uriFactory->createUri(self::HOST.'decks/archetype/'.((string) $archetypeId).'/card/'.((string) $printingId).'?page='.((string) $pagenb));
		$request = $this->_requestFactory->createRequest('GET', $uri);
		$response = $this->_httpClient->sendRequest($request);
		$json = new JsonStringDataProvider($response->getBody()->__toString());
		
		return $this->_reifier->reifyAll(ApiComMtgstocksDeckAttributes::class, $json->provideAll());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\ApiComMtgstocks\ApiComMtgstocksEndpointInterface::getDecksByArchetype()
	 * @throws ClientExceptionInterface
	 * @throws InvalidArgumentException should not happen
	 * @throws ReificationThrowable
	 * @throws UnprovidableThrowable
	 */
	public function getDecksByArchetype(int $archetypeId, int $pagenb = 1) : ApiComMtgstocksArchetypePrez
	{
		
		$uri = $this->_uriFactory->createUri(self::HOST.'archetypes/'.((string) $archetypeId).'?page='.((string) $pagenb));
		$request = $this->_requestFactory->createRequest('GET', $uri);
		$response = $this->_httpClient->sendRequest($request);
		$json = new JsonStringDataProvider($response->getBody()->__toString());
		
		return $this->_reifier->reify(ApiComMtgstocksArchetypePrez::class, $json->provideOne());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\ApiComMtgstocks\ApiComMtgstocksEndpointInterface::getTournament()
	 * @throws ClientExceptionInterface
	 * @throws InvalidArgumentException should not happen
	 * @throws ReificationThrowable
	 * @throws UnprovidableThrowable
	 */
	public function getTournament(int $tournamentId) : ApiComMtgstocksTournament
	{
		$uri = $this->_uriFactory->createUri(self::HOST.'tournaments/'.((string) $tournamentId));
		$request = $this->_requestFactory->createRequest('GET', $uri);
		$response = $this->_httpClient->sendRequest($request);
		$json = new JsonStringDataProvider($response->getBody()->__toString());
		
		return $this->_reifier->reify(ApiComMtgstocksTournament::class, $json->provideOne());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\ApiComMtgstocks\ApiComMtgstocksEndpointInterface::getTournamentsByFormat()
	 * @throws ClientExceptionInterface
	 * @throws InvalidArgumentException should not happen
	 * @throws ReificationThrowable
	 * @throws UnprovidableThrowable
	 */
	public function getTournamentsByFormat(int $formatId, int $pagenb = 1) : array
	{
		$uri = $this->_uriFactory->createUri(self::HOST.'tournaments/format/'.((string) $formatId).'?page='.((string) $pagenb));
		$request = $this->_requestFactory->createRequest('GET', $uri);
		$response = $this->_httpClient->sendRequest($request);
		$json = new JsonStringDataProvider($response->getBody()->__toString());
		
		return $this->_reifier->reifyAll(ApiComMtgstocksTournamentPrez::class, $json->provideAll());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\ApiComMtgstocks\ApiComMtgstocksEndpointInterface::getArchetypesByFormat()
	 * @throws ClientExceptionInterface
	 * @throws InvalidArgumentException should not happen
	 * @throws ReificationThrowable
	 * @throws UnprovidableThrowable
	 */
	public function getArchetypesByFormat(int $formatId) : array
	{
		$uri = $this->_uriFactory->createUri(self::HOST.'archetypes/format/'.((string) $formatId));
		$request = $this->_requestFactory->createRequest('GET', $uri);
		$response = $this->_httpClient->sendRequest($request);
		$json = new JsonStringDataProvider($response->getBody()->__toString());
		
		return $this->_reifier->reifyAll(ApiComMtgstocksArchetype::class, $json->provideAll());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\ApiComMtgstocks\ApiComMtgstocksEndpointInterface::getMetagame()
	 * @throws ClientExceptionInterface
	 * @throws InvalidArgumentException should not happen
	 * @throws ReificationThrowable
	 * @throws UnprovidableThrowable
	 */
	public function getMetagame(int $formatId) : array
	{
		$uri = $this->_uriFactory->createUri(self::HOST.'analytics/metagame/'.((string) $formatId));
		$request = $this->_requestFactory->createRequest('GET', $uri);
		$response = $this->_httpClient->sendRequest($request);
		$json = new JsonStringDataProvider($response->getBody()->__toString());
		
		return $this->_reifier->reifyAll(ApiComMtgstocksMetagameShare::class, $json->provideAll());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\ApiComMtgstocks\ApiComMtgstocksEndpointInterface::getFormats()
	 * @throws ClientExceptionInterface
	 * @throws InvalidArgumentException should not happen
	 * @throws ParseThrowable should not happen
	 * @throws ReificationThrowable
	 * @throws RuntimeException
	 * @throws UnprovidableThrowable
	 */
	public function getFormats() : array
	{
		if([] !== $this->_formats)
		{
			return $this->_formats;
		}
		
		$url = 'https://www.mtgstocks.com';
		
		try
		{
			$uri = $this->_uriFactory->createUri($url);
			$request = $this->_requestFactory->createRequest('GET', $uri);
			$response = $this->_httpClient->sendRequest($request);
			$contents = $response->getBody()->__toString();
		}
		// @codeCoverageIgnoreStart
		catch(Throwable $e)
		{
			$message = 'Failed to parse uri {uri}';
			$context = ['{uri}' => $url];
			
			throw new InvalidArgumentException(\strtr($message, $context));
		}
		// @codeCoverageIgnoreEnd
		
		$needle = '<script src="main.';
		$pos1 = \mb_strpos($contents, $needle);
		if(false === $pos1)
		{
			// @codeCoverageIgnoreStart
			$message = 'Failed to find main bundle url at url "{url}".';
			$context = ['{url}' => $uri->__toString()];
			
			throw new RuntimeException(\strtr($message, $context));
			// @codeCoverageIgnoreEnd
		}
		$pos2 = \mb_strpos($contents, '.js"', $pos1 + 1);
		if(false === $pos2)
		{
			// @codeCoverageIgnoreStart
			$message = 'Failed to find main bundle url end at url "{url}".';
			$context = ['{url}' => $uri->__toString()];
			
			throw new RuntimeException(\strtr($message, $context));
			// @codeCoverageIgnoreEnd
		}
		
		$bundleHash = (string) \mb_substr($contents, $pos1 + (int) \mb_strlen($needle), $pos2 - $pos1 - (int) \mb_strlen($needle));
		
		$url = 'https://www.mtgstocks.com/main.'.$bundleHash.'.js';
		$uri = $this->_uriFactory->createUri($url);
		$request = $this->_requestFactory->createRequest('GET', $uri);
		$response = $this->_httpClient->sendRequest($request);
		$contents = $response->getBody()->__toString();
		
		$needle = 'legalFormats(){return[';
		$pos1 = \mb_strpos($contents, $needle);
		if(false === $pos1)
		{
			// @codeCoverageIgnoreStart
			$message = 'Failed to find legal formats in application bundle at url "{url}".';
			$context = ['{url}' => $url];
			
			throw new RuntimeException(\strtr($message, $context));
			// @codeCoverageIgnoreEnd
		}
		$pos2 = \mb_strpos($contents, ']', $pos1 + 1);
		if(false === $pos2)
		{
			// @codeCoverageIgnoreStart
			$message = 'Failed to find legal format end in application bundle at url "{url}".';
			$context = ['{url}' => $url];
			
			throw new RuntimeException(\strtr($message, $context));
			// @codeCoverageIgnoreEnd
		}
		
		$formats = (string) \mb_substr($contents, $pos1 + (int) \mb_strlen($needle) - 1, $pos2 - $pos1 - ((int) \mb_strlen($needle) - 2));
		
		// json rectifications from pure js
		$formats = \str_replace('id:', '"id":', $formats);
		$formats = \str_replace('name:', '"name":', $formats);
		$provider = new JsonStringDataProvider($formats);
		
		return $this->_formats = $this->_reifier->reifyAll(ApiComMtgstocksFormat::class, $provider->provideAll());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\ApiComMtgstocks\ApiComMtgstocksEndpointInterface::getInterests()
	 * @throws ClientExceptionInterface
	 * @throws InvalidArgumentException should not happen
	 * @throws ReificationThrowable
	 * @throws UnprovidableThrowable
	 */
	public function getInterests() : ApiComMtgstocksInterests
	{
		$uri = $this->_uriFactory->createUri(self::HOST.'interests/average');
		$request = $this->_requestFactory->createRequest('GET', $uri);
		$response = $this->_httpClient->sendRequest($request);
		$json = new JsonStringDataProvider($response->getBody()->__toString());
		
		return $this->_reifier->reify(ApiComMtgstocksInterests::class, $json->provideOne());
	}
	
}
