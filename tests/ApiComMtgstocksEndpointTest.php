<?php declare(strict_types=1);

/*
 * This file is part of the php-mtg/mtg-api-com-mtgstocks-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\HttpMessage\Response;
use PhpExtended\HttpMessage\StringStream;
use PhpMtg\ApiComMtgstocks\ApiComMtgstocksArchetype;
use PhpMtg\ApiComMtgstocks\ApiComMtgstocksArchetypeStat;
use PhpMtg\ApiComMtgstocks\ApiComMtgstocksDeck;
use PhpMtg\ApiComMtgstocks\ApiComMtgstocksDeckAttributes;
use PhpMtg\ApiComMtgstocks\ApiComMtgstocksEndpoint;
use PhpMtg\ApiComMtgstocks\ApiComMtgstocksFormat;
use PhpMtg\ApiComMtgstocks\ApiComMtgstocksLink;
use PhpMtg\ApiComMtgstocks\ApiComMtgstocksMetagameShare;
use PhpMtg\ApiComMtgstocks\ApiComMtgstocksPriceHistory;
use PhpMtg\ApiComMtgstocks\ApiComMtgstocksPrinting;
use PhpMtg\ApiComMtgstocks\ApiComMtgstocksPrintingPrez;
use PhpMtg\ApiComMtgstocks\ApiComMtgstocksSetPrez;
use PhpMtg\ApiComMtgstocks\ApiComMtgstocksTournament;
use PhpMtg\ApiComMtgstocks\ApiComMtgstocksTournamentPrez;
use PHPUnit\Framework\TestCase;
use Psr\Http\Client\ClientInterface;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

/**
 * ApiComMtgstocksEndpointTest class file.
 * 
 * @author Anastaszor
 * @covers \PhpMtg\ApiComMtgstocks\ApiComMtgstocksEndpoint
 *
 * @internal
 *
 * @small
 */
class ApiComMtgstocksEndpointTest extends TestCase
{
	
	/**
	 * The endpoint to test.
	 * 
	 * @var ApiComMtgstocksEndpoint
	 */
	protected ApiComMtgstocksEndpoint $_endpoint;
	
	/**
	 * public function testFormats() : void
	 * {
	 * foreach($this->_endpoint->getFormats() as $format)
	 * {
	 * $this->assertInstanceOf(ApiComMtgstocksFormat::class, $format);
	 * }.
	 *
	 * // fire local cache
	 * $this->_endpoint->getFormats();
	 * }
	 *
	 * public function testSetsInformation() : void
	 * {
	 * foreach($this->_endpoint->getSetsInformation() as $set)
	 * {
	 * $this->assertInstanceOf(ApiComMtgstocksSetPrez::class, $set);
	 * }
	 * }
	 *
	 * public function testSet10thEditionInformation() : void
	 * {
	 * $set = $this->_endpoint->getSetPrintings(65);
	 *
	 * foreach($set->prints as $print)
	 * {
	 * $this->assertInstanceOf(ApiComMtgstocksPrintingPrez::class, $print);
	 * }
	 * }
	 *
	 * public function testGet10EForest307Information() : void
	 * {
	 * $print = $this->_endpoint->getPrinting(40404);
	 *
	 * $this->assertInstanceOf(ApiComMtgstocksPrinting::class, $print);
	 * }
	 *
	 * public function testGet10EForest307Prices() : void
	 * {
	 * $print = $this->_endpoint->getPrintingPrices(40404);
	 *
	 * $this->assertInstanceOf(ApiComMtgstocksPriceHistory::class, $print);
	 * }
	 *
	 * public function testGet10EForest307Links() : void
	 * {
	 * $links = $this->_endpoint->getPrintingLinks(11864); // the rack
	 *
	 * foreach($links as $link)
	 * {
	 * $this->assertInstanceOf(ApiComMtgstocksLink::class, $link);
	 * }
	 * }
	 *
	 * public function testGetJaceTheMindSculptorDecks() : void
	 * {
	 * $decks = $this->_endpoint->getCardArchetypePresences(1549);
	 *
	 * foreach($decks as $deck)
	 * {
	 * $this->assertInstanceOf(ApiComMtgstocksArchetypeStat::class, $deck);
	 * }
	 * }
	 *
	 * public function testGetJaceControlDeck() : void
	 * {
	 * $deck = $this->_endpoint->getDeck(601);
	 *
	 * $this->assertInstanceOf(ApiComMtgstocksDeck::class, $deck);
	 * }
	 *
	 * public function testGetDecksByArchetypeAndPrintingTemurWithShardlessAgent() : void
	 * {
	 * foreach($this->_endpoint->getDecksByArchetypeAndPrinting(3220, 12785) as $deck)
	 * {
	 * $this->assertInstanceOf(ApiComMtgstocksDeckAttributes::class, $deck);
	 * }
	 * }
	 *
	 * public function testGetDecksByArchetypeTezzerator() : void
	 * {
	 * foreach($this->_endpoint->getDecksByArchetype(15)->decks as $deck)
	 * {
	 * $this->assertInstanceOf(ApiComMtgstocksDeckAttributes::class, $deck);
	 * }
	 * }
	 *
	 * public function testGetTournamentStarcityGamesLegacyOpenMilwaukee() : void
	 * {
	 * $this->assertInstanceOf(ApiComMtgstocksTournament::class, $this->_endpoint->getTournament(500));
	 * }
	 *
	 * public function testGetTournamentsByFormatStandardLeague() : void
	 * {
	 * foreach($this->_endpoint->getTournamentsByFormat(4) as $tournament)
	 * {
	 * $this->assertInstanceOf(ApiComMtgstocksTournamentPrez::class, $tournament);
	 * }
	 * }
	 *
	 * public function testGetArchetypesByFormatLegacy() : void
	 * {
	 * foreach($this->_endpoint->getArchetypesByFormat(2) as $archetype)
	 * {
	 * $this->assertInstanceOf(ApiComMtgstocksArchetype::class, $archetype);
	 * }
	 * }
	 *
	 * public function testGetMetagameModern() : void
	 * {
	 * foreach($this->_endpoint->getMetagame(3) as $metagame)
	 * {
	 * $this->assertInstanceOf(ApiComMtgstocksMetagameShare::class, $metagame);
	 * }
	 * }
	 *
	 * // disable this method as it fails most of the time (response take too long)
	 * // 	public function testGetInterests() : void
	 * // 	{
	 * // 		$this->assertInstanceOf(ApiComMtgstocksInterests::class, $this->_endpoint->getInterests());
	 * // 	}
	 *
	 * /**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$client = new class() implements ClientInterface
		{
			
			public function sendRequest(RequestInterface $request) : ResponseInterface
			{
				\sleep(1);
				$options = ['http' => ['user_agent' => 'Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:47.0) Gecko/20100101 Firefox/47.0']];
				$data = @\file_get_contents($request->getUri()->__toString(), false, \stream_context_create($options));
				if(false === $data)
				{
					\sleep(1);
					$options = ['http' => ['user_agent' => 'Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:47.0) Gecko/20100101 Firefox/47.0']];
					$data = @\file_get_contents($request->getUri()->__toString(), false, \stream_context_create($options));
					if(false === $data)
					{
						\sleep(1);
						$options = ['http' => ['user_agent' => 'Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:47.0) Gecko/20100101 Firefox/47.0']];
						$data = \file_get_contents($request->getUri()->__toString(), false, \stream_context_create($options));
					}
				}
				$body = new StringStream($data);
				
				return (new Response())->withBody($body);
			}
		};
		
		$this->_endpoint = new ApiComMtgstocksEndpoint($client);
	}
	
}
