# php-mtg/mtg-api-com-mtgstocks-object

A library that implements the php-mtg/mtg-api-com-mtgstocks-interface library.

![coverage](https://gitlab.com/php-mtg/mtg-api-com-mtgstocks-object/badges/master/pipeline.svg?style=flat-square)
![build status](https://gitlab.com/php-mtg/mtg-api-com-mtgstocks-object/badges/master/coverage.svg?style=flat-square)


## Installation

The installation of this library is made via composer and the autoloading of
all classes of this library is made through their autoloader.

- Download `composer.phar` from [their website](https://getcomposer.org/download/).
- Then run the following command to install this library as dependency :
- `php composer.phar php-mtg/mtg-api-com-mtgstocks-object ^8`


## Basic Usage

This library may be used the following way :

```php

use PhpMtg\Mtgstocks\MtgstocksApiEndpoint;

/* @var $client \Psr\Http\Client\ClientInterface */

$endpoint = new MtgstocksApiEndpoint($client);

$collection = $this->_endpoint->getSetsInformation();

foreach($collection as $setResume)
{
	/** @var \PhpMtg\Mtgstocks\MtgstocksApiSetPrezInterface $meta */
	$set = $this->_endpoint->getSet($setResume->getId());
	
	// do something with set
}

```


## License

MIT (See [license file](LICENSE)).
